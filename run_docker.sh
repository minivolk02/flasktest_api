echo killing old docker processes
docker-compose rm -fs

echo create requirements for docker api
pipenv lock --keep-outdated --requirements > api/prod-requirements.txt

echo building docker containers
docker-compose up --build -d
