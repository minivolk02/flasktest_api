from flask_restplus import Api
from flask import Blueprint

from .main.resources.camera_resource import cameras_ns, camera_ns
from .main.resources.area_resource import areas_ns, area_ns
from .main.resources.controller_resource import controllers_ns, controller_ns

blueprint = Blueprint('api', __name__, url_prefix='/api')

api = Api(blueprint,
          title='Camera Motion Detect Flask API',
          version='1.0',
          doc='/doc'
          )

api.add_namespace(cameras_ns, path='/cameras')
api.add_namespace(camera_ns, path='/camera')
api.add_namespace(areas_ns, path='/areas')
api.add_namespace(area_ns, path='/area')
api.add_namespace(controllers_ns, path='/controllers')
api.add_namespace(controller_ns, path='/controller')
