from flask import request

from app.main import mqtt
from flask_restplus import Resource, Namespace, fields

from ..services.controller_service import get_all_controllers, get_a_controller, change_a_controller, \
    delete_a_controller

controllers_ns = Namespace('controllers', description='Controllers related operations')
controller_ns = Namespace('controller', description='Controller related operations')

controller = controllers_ns.model('Controller', {
    'uid': fields.String,
    'status': fields.Integer,
    'area_id': fields.Integer
})


class ControllerList(Resource):
    def get(self, scan=None):
        if scan is None:
            return get_all_controllers()
        else:
            mqtt.publish('controllers/scan', 1)
            return {'message': 'Scan on'}, 200


class ControllerDetail(Resource):
    def get(self, controller_id):
        return get_a_controller(controller_id)

    @controllers_ns.expect(controller)
    def put(self, controller_id):
        json_data = request.get_json(force=True)
        return change_a_controller(controller_id, json_data)

    def delete(self, controller_id):
        return delete_a_controller(controller_id)


controllers_ns.add_resource(ControllerList, '', '/<scan>')
controller_ns.add_resource(ControllerDetail, '/<int:controller_id>')
