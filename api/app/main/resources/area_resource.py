from flask import request
from flask_restplus import Resource, Namespace, fields
from ..services.area_service import get_all_areas, get_an_area, save_new_area, change_a_area, delete_a_area

areas_ns = Namespace('areas', description='Cameras related operations')
area_ns = Namespace('area', description='Camera related operations')

area = areas_ns.model('Area', {
    'coordinates': fields.List(fields.List(fields.Float)),
    'camera_id': fields.Integer,
    'controllers': fields.List(fields.String)
})


class AreaList(Resource):
    def get(self):
        return get_all_areas()


class AreaDetail(Resource):
    def get(self, area_id):
        return get_an_area(area_id)

    @area_ns.expect(area)
    def post(self):
        json_data = request.get_json()
        return save_new_area(json_data)

    @area_ns.expect(area)
    def put(self, area_id):
        json_data = request.get_json()
        return change_a_area(area_id, json_data)

    def delete(self, area_id):
        return delete_a_area(area_id)


areas_ns.add_resource(AreaList, '')
area_ns.add_resource(AreaDetail, '', '/<int:area_id>')
