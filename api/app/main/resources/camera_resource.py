from flask import request
from flask_restplus import Resource, Namespace, fields
from ..services.camera_service import get_all_cameras, get_a_camera, save_new_camera, capture_stream, change_a_camera, \
    delete_a_camera

cameras_ns = Namespace('cameras', description='Cameras related operations')
camera_ns = Namespace('camera', description='Camera related operations')

camera = cameras_ns.model('Camera', {
    'address': fields.String,
    'username': fields.String,
    'password': fields.String,
    'numeric_dataset': fields.List(fields.Float),
    'isomeric_dataset': fields.List(fields.Float)
})


class CameraList(Resource):
    def get(self):
        return get_all_cameras()


class CameraDetail(Resource):
    def get(self, camera_id, capture=None):
        if capture is None:
            return get_a_camera(camera_id)
        else:
            capture_stream(camera_id)

    @camera_ns.expect(camera)
    def post(self):
        json_data = request.get_json()
        return save_new_camera(json_data)

    @camera_ns.expect(camera)
    def put(self, camera_id):
        json_data = request.get_json()
        return change_a_camera(camera_id, json_data)

    def delete(self, camera_id):
        return delete_a_camera(camera_id)


cameras_ns.add_resource(CameraList, '')
camera_ns.add_resource(CameraDetail, '', '/<int:camera_id>', '/<int:camera_id>/<capture>')
