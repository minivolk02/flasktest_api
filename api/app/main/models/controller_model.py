from .. import db, ma
from sqlalchemy.dialects.postgresql import UUID
from marshmallow import fields, validate

from .base_model import BaseMixin


class Controller(BaseMixin, db.Model):
    __tablename__ = 'controllers'

    uid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)
    status = db.Column(db.Integer, default=0)
    area_id = db.Column(db.Integer, db.ForeignKey('areas.id'))

    def __repr__(self):
        return "<Controller {}>".format(self.id)


class ControllerSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Controller
        load_instance = True
        include_fk = True

    uid = fields.String(required=False)
    status = fields.Integer(required=False, validate=validate.Range(0, 100))
