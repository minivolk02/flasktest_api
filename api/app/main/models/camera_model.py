from .. import db, ma

from sqlalchemy_utils import IPAddressType
from sqlalchemy.dialects.postgresql import ARRAY

from marshmallow import fields, validate

from .base_model import BaseMixin


class Camera(BaseMixin, db.Model):
    __tablename__ = 'cameras'

    address = db.Column(IPAddressType, unique=True)
    username = db.Column(db.String(30), nullable=True)
    password = db.Column(db.String(30), nullable=True)

    numeric_dataset = db.Column(ARRAY(db.Float), nullable=True)
    isomeric_dataset = db.Column(ARRAY(db.Float), nullable=True)

    def get_url(self):
        return "rtsp://{}:{}@{}".format(self.username, self.password, self.address)

    def __repr__(self):
        return "<Camera {}>".format(self.id)


class CameraSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Camera
        load_instance = True
        include_fk = True

    numeric_dataset = fields.List(fields.Float(), validate=validate.Length(4, 4))
    isomeric_dataset = fields.List(fields.Float(), validate=validate.Length(4, 4))
