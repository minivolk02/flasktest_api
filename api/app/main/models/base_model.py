from typing import List

from sqlalchemy.exc import IntegrityError

from .. import db


class BaseMixin(object):
    id = db.Column(db.Integer, primary_key=True)

    @classmethod
    def find_by_id(cls, _id) -> "object":
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def find_all(cls) -> List["object"]:
        return cls.query.all()

    def save_to_db(self) -> None:
        try:
            db.session.add(self)
            db.session.commit()
        except IntegrityError:
            db.session.rollback()

    def delete_from_db(self) -> None:
        db.session.delete(self)
        db.session.commit()
