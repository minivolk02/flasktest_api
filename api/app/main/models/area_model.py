import uuid

from .. import db, ma
from sqlalchemy.dialects.postgresql import UUID, ARRAY
from marshmallow import fields, validate
from .controller_model import Controller

from .base_model import BaseMixin


class Area(BaseMixin, db.Model):
    __tablename__ = 'areas'

    uid = db.Column(UUID(as_uuid=True), unique=True, default=uuid.uuid4, nullable=False)
    coordinates = db.Column(ARRAY(db.Float), nullable=False)
    camera_id = db.Column(db.Integer, db.ForeignKey('cameras.id'), nullable=True)
    controllers = db.relationship(Controller, backref='controller', lazy='dynamic')

    def __repr__(self):
        return "<Area {}>".format(self.id)


class AreaSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Area
        load_instance = True
        include_fk = True

    coordinates = fields.List(fields.List(fields.Float()), validate=validate.Length(3))
