from app.main.models.camera_model import Camera, CameraSchema
from flask import Response

from app.main.utils.capture import gen_frames

camera_schema = CameraSchema()
cameras_schema = CameraSchema(many=True)


def save_new_camera(json_data):
    check_camera = Camera.query.filter_by(address=json_data['address']).first()
    if not check_camera:
        new_camera = Camera(**json_data)
        new_camera.save_to_db()
        response_object = camera_schema.dump(new_camera)
        return response_object, 201

    else:
        response_object = {
            'message': 'Camera with same address already exists!',
        }
        return response_object, 409


def get_all_cameras():
    cameras = Camera.find_all()
    if not cameras:
        return {'message': 'No cameras found!'}, 404
    else:
        cameras = cameras_schema.dump(cameras)
        return cameras, 200


def get_a_camera(pk):
    camera = Camera.find_by_id(pk)
    if camera:
        camera = camera_schema.dump(camera)
        return camera, 200
    else:
        return {'message': 'Camera doesnt exists!'}, 409


def capture_stream(pk):
    camera = Camera.find_by_id(pk)
    print(camera)
    if camera:
        return Response(gen_frames(camera.get_url()),
                        mimetype='multipart/x-mixed-replace; boundary=frame')


def change_a_camera(pk, json_data):
    camera = Camera.find_by_id(pk)
    if camera:
        if json_data['address']:
            camera.status = json_data['address']
        if json_data['username']:
            camera.area_id = json_data['username']
        if json_data['password']:
            camera.area_id = json_data['password']
        if json_data['numeric_dataset']:
            camera.area_id = json_data['numeric_dataset']
        if json_data['isomeric_dataset']:
            camera.area_id = json_data['isomeric_dataset']
        camera.save_to_db()
        camera = camera_schema.dump(camera)
        return camera, 200


def delete_a_camera(pk):
    camera = Camera.find_by_id(pk)
    camera.delete_from_db()
    return {"message": "Successfully deleted!"}
