from app.main.models.area_model import Area, AreaSchema

area_schema = AreaSchema()
areas_schema = AreaSchema(many=True)


def save_new_area(json_data):
    new_area = Area(**json_data)
    new_area.save_to_db()
    response_object = area_schema.dump(new_area)
    return response_object, 201


def get_all_areas():
    areas = Area.find_all()
    if not areas:
        return {'message': 'No areas found!'}, 404
    else:
        areas = areas_schema.dump(areas)
        return areas, 200


def get_an_area(pk):
    area = Area.find_by_id(pk)
    if area:
        area = area_schema.dump(area)
        return area, 200
    else:
        return {'message': 'Area doesnt exists!'}, 409


def change_a_area(pk, json_data):
    area = Area.find_by_id(pk)
    if area:
        if json_data['coordinates']:
            area.coordinates = json_data['coordinates']
        if json_data['camera_id']:
            area.coordinates = json_data['camera_id']
        if json_data['controllers']:
            area.coordinates = json_data['controllers']
        area.save_to_db()
        area = area_schema.dump(area)
        return area, 200


def delete_a_area(pk):
    area = Area.find_by_id(pk)
    area.delete_from_db()
    return {"message": "Successfully deleted!"}
