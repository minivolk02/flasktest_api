from app.main.models.controller_model import Controller, ControllerSchema
from app.main import mqtt, create_app

controller_schema = ControllerSchema()
controllers_schema = ControllerSchema(many=True)


def save_new_controller(schema_data):
    check_controller = Controller.query.filter_by(uid=schema_data.uid).first()
    if not check_controller:
        schema_data.save_to_db()
        response_object = controller_schema.dump(schema_data)
        return response_object, 201
    else:
        return {'message': 'Controller with same uid already exists!'}, 409


def get_all_controllers():
    controllers = Controller.find_all()
    if not controllers:
        return {'message': 'No controllers found!'}, 404
    else:
        controllers = controllers_schema.dump(controllers)
        return controllers, 200


def get_a_controller(pk):
    controller = Controller.find_by_id(pk)
    if controller:
        controller = controller_schema.dump(controller)
        return controller, 200
    else:
        return {'message': 'Controller doesnt exists!'}, 409


def delete_a_controller(pk):
    controller = Controller.find_by_id(pk)
    controller.delete_from_db()
    return {"message": "Successfully deleted!"}


def change_a_controller(pk, json_data):
    controller = Controller.find_by_id(pk)
    schema_data = controller_schema.load(json_data)
    if controller:
        if schema_data.status:
            controller.status = schema_data.status
        if schema_data.area_id:
            controller.area_id = schema_data.area_id
        controller.save_to_db()
        controller = controller_schema.dump(controller)
        return controller, 200


@mqtt.on_connect()
def handle_connect(client, userdata, flags, rc):
    mqtt.subscribe('controller/#')


@mqtt.on_message()
def handle_mqtt_message(client, userdata, message):
    from api.manage import app
    topic = message.topic.split('/')
    payload = message.payload.decode()

    new_controller = controller_schema.load({"uid": topic[1], "status": int(payload)})
    if new_controller:
        with app.app_context():
            save_new_controller(new_controller)
