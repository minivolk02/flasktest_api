#
# These requirements were autogenerated by pipenv
# To regenerate from the project's Pipfile, run:
#
#    pipenv lock --requirements
#

-i https://pypi.org/simple
alembic==1.5.5; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4, 3.5'
aniso8601==9.0.1
attrs==20.3.0; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
bcrypt==3.2.0; python_version >= '3.6'
cffi==1.14.5
click==7.1.2; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4'
flask-bcrypt==0.7.1
flask-marshmallow==0.14.0
flask-migrate==2.7.0
flask-mqtt==1.1.1
flask-restplus==0.13.0
flask-script==2.0.6
flask-sqlalchemy==2.4.4
flask==1.1.2
gunicorn==20.0.4
importlib-metadata==3.7.0; python_version < '3.8'
itsdangerous==1.1.0; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
jinja2==2.11.3; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4'
jsonschema==3.2.0
mako==1.1.4; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
markupsafe==1.1.1; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
marshmallow-sqlalchemy==0.24.2
marshmallow==3.10.0; python_version >= '3.5'
numpy==1.20.1; python_version >= '3.7'
opencv-python==4.5.1.48
paho-mqtt==1.5.1
psycopg2-binary==2.8.6
pycparser==2.20; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
pyrsistent==0.17.3; python_version >= '3.5'
python-dateutil==2.8.1; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
python-dotenv==0.15.0
python-editor==1.0.4
pytz==2021.1
six==1.15.0; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
sqlalchemy-utils==0.36.8
sqlalchemy==1.3.23
typing-extensions==3.7.4.3; python_version < '3.8'
werkzeug==0.16.1
zipp==3.4.0; python_version >= '3.6'
